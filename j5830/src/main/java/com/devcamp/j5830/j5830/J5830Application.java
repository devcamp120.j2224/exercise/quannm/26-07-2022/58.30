package com.devcamp.j5830.j5830;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J5830Application {

	public static void main(String[] args) {
		SpringApplication.run(J5830Application.class, args);
	}

}
