package com.devcamp.j5830.j5830.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j5830.j5830.model.CCustomer;
import com.devcamp.j5830.j5830.repository.ICustomerRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CCustomerController {
    @Autowired
    ICustomerRepository pCustomerRepository;

    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllCustomers() {
        try {
            List<CCustomer> listCustomers = new ArrayList<CCustomer>();

            pCustomerRepository.findAll().forEach(listCustomers::add);

            return new ResponseEntity<List<CCustomer>>(listCustomers, HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
