package com.devcamp.j5830.j5830.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j5830.j5830.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {
    
}
